require 'rubygems'
require 'mongo'
require 'time'
require 'rest-client'
require 'json'
require 'colorize'
require 'date'

db = Mongo::Client.new([ "127.0.0.1:27017" ], :database => "data_collector_development")

logs = db[:sensor_values].find({"capability": "edge_monitoring"})
logs.find().update({"$set" => {:cluster => (self.origin.lat < 1.0) }}, {:multi => true})
