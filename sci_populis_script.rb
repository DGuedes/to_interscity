require 'rubygems'
require 'mongo'
require 'time'
require 'rest-client'
require 'json'
require 'colorize'
require 'date'

db = Mongo::Client.new([ "127.0.0.1:27017" ], :database => "meteor")

# logs = db[:new_logs_incomplete].find({"begin_ts": { "$gt": Date.parse("2017-10-23") }}).projection({
logs = db[:new_logs_incomplete].find({}).projection({
  begin_ts: 1,
  ts_mark: 1,
  "stops.avg_speed": 1,
  "stops.ts_mark": 1,
  "stops.shape.loc": 1,
  ref_hour: 1
})

# puts logs.count()

edges = {edges: []}

logs.each do |p|
  if p.nil?
    next
  end
  stops_len = p[:stops].size
  if (p[:stops].nil? || p[:stops].empty?)
    next
  end

  # puts "p => #{p}"
  p[:stops].each_with_index do |st, idx|
    if st.nil?
      next
    end

    ts = st[:begin_ts]
    if ts == nil
      ts = st[:ts_mark]
    end
    vm = st[:avg_speed]

    shape = {}

    if idx == stops_len - 1
      break
    end

    # puts "st => #{st}"

    if (st[:shape].nil? || st[:shape].empty?)
      next
    end
    shape_len = st[:shape].size
    # puts "shape_len => #{shape_len}"

    shape[:avg_speed] = vm

    shape[:ref_hour] = p[:ref_hour]
    shape[:timestamp] = ts

    shape[:origin] = {}
    shape[:origin][:lat] = st[:shape][0][:loc][:lat]
    shape[:origin][:lon] = st[:shape][0][:loc][:lng]
    shape[:destination] = {}
    # puts "shape_len => #{shape_len}"
    # puts "st => #{st}"
    shape[:destination][:lat] = st[:shape][shape_len-1][:loc][:lat]
    shape[:destination][:lon] = st[:shape][shape_len-1][:loc][:lng]

    # puts "shape => #{shape}"

    edges[:edges].push shape
  end
end
# exit(0)

puts "COUNT => #{edges[:edges].count}"

edges[:edges].drop(263525).each do |e|
  # register resource
  url = "localhost:8000/adaptor/components"
  doc = {
    lat: e[:origin][:lat],
    lon: e[:origin][:lon],
    description: "Edge from point [#{e[:origin][:lat]}, #{e[:origin][:lon]}] to [#{e[:destination][:lat]}, #{e[:destination][:lon]}",
    capabilities: ["edge_monitoring"],
    status: "active"
  }
  response = RestClient.post(url, {data: doc})
  response = JSON.parse(response)
  uuid = response["data"]["uuid"]
  puts "Resource #{uuid} #{'registered'.green}"

  # send data
  url = "localhost:8000/adaptor/components/#{uuid}/data"
  doc = {
    edge_monitoring: [{
      origin: {
        lat: e[:origin][:lat],
        lon: e[:origin][:lon]
      },
      destination: {
        lat: e[:destination][:lat],
        lon: e[:destination][:lon]
      },
      avg_speed: e[:avg_speed],
      timestamp: e[:timestamp],
      ref_hour: e[:ref_hour]
    }]
  }
  response = RestClient.post(url, {data: doc})
  # exit(0)
  puts "Resource #{uuid} #{'updated'.blue}"
end
